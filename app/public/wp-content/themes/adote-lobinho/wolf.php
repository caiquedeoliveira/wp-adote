<?php
// Template Name: Single Lobinho
?>

<?php get_header(); ?>
    
    <main>
    
            <li class="wolf-page">
            <?php if( get_field('foto') ): ?>
                <div class="wolf-picture">
                    <img class="wolf-image" src="<?php the_field('foto'); ?>" />
                </div>
                
            <?php endif; ?>
            
            <div class="wolf-text">
                <div class="wolf-info">
                    <div class="name-age">
                        <h3 class="wolf-name"><?php the_field('nome_do_lobo'); ?></h3>
                        <p class="wolf-age">Idade: <?php the_field('idade'); ?> anos</p>
                    </div>
                    <div class="adopt-button">
                        <button>Ver</button>
                    </div>
                </div>
                <p class="wolf-description"><?php the_field('sobre_o_lobo'); ?> </p> 
            </div>
            </li>

    </main>

<?php get_footer(); ?>

