<?php
// Template Name: Quem Somos | Adote Lobinho
?>

<?php get_header(); ?>
    <main>
        <section class="about-us">
            <h1><?php the_field('titulo'); ?></h1>
                <p>
                <?php the_field('descricao'); ?> 
                </p>
        </section>
    </main>
<?php get_footer(); ?>