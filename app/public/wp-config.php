<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5xve7AYCyUW1wWWGv2LUSwnJ539byqXH3bAvxK6wpknIc33FtQrEIPOmLucQhCWgnQ3StSGv7JcrTbIfoH4KTQ==');
define('SECURE_AUTH_KEY',  '8TaN5Ss20Q39lu+b9OpJndqyd10UrgUReJp8NR+xOP4qirfzJTJiq8DsfQ3SSzjbuJpiOLKJZbgqaMD9JvU8rQ==');
define('LOGGED_IN_KEY',    'dBqSW8kMIyhollvD5sV+00JT907wCIDLBwy0ddGiv83mApZbsFa+zPa/8ESapFferpGeYmpzhmyoIEu94aKhzg==');
define('NONCE_KEY',        'iHNZmAqi/HdkrNQFJgw3/pCEthW5wi/Mm8P9b1MUJaMm4tQLea/fm54ru0cc4csVVjzM796NZMgJ2gOu8zRzNA==');
define('AUTH_SALT',        '20LfusJeo6hlpA4IcOYy/Z7Zh7DFFugh6FHbBrCN/vEN6A/lUEaNK5XjPC+l8yhG6oR+8vBiuAGQYBtcjPxuDQ==');
define('SECURE_AUTH_SALT', 'ezuy2cWmirf7fx87f+hcV9Q/zZzzG1bVvcIa96nc35cyfgtHI3zQ4QRWd+wWYmc5ACw+ycfgxg3qxa+19yNA9g==');
define('LOGGED_IN_SALT',   'DiIP7+/ar4gVcdR2E8lwCm2R0ZUaACDnoQar2fVlnNaJUmF5+ZmWmUobir9jr/lYUr126yOV1HDELBrEse3Hcg==');
define('NONCE_SALT',       'vTxR1t9lfstD8o9AhDxQ0QKHXdLzeY8hb7jI8t8k2TbwXsovw6cyGxA24iPLPaYGek0XF5/r+nlCYtmCoqQqqA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
